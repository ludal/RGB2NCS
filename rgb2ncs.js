const to2nr = nr => {
    if(nr<=16) "0"+nr;
    else nr;
}

const decimalToHex = dec => {
    hex = dec.toString(16)
    if (dec <= 16)
        hex = '0' + hex
    hex 
    
}
const hexToDecimal = hex => to2nr(parseInt(hex, 16));



function ncsToRgbHash(ncs) {
    rgb = ncsToRgb(ncs)
    r = "#" + decimalToHex(rgb.r) + decimalToHex(rgb.g) + decimalToHex(rgb.b)
    //console.log(r)
    return r
}

function num2(i) {
    if (i < 10) return "0" + i
    if (i < 100) return i
    else return 99
}

function check(colorToFind) {
    colorToFind = colorToFind.toLowerCase();
    const ncsMap = new Map()
    const max = 90
    //const max = 9

    for (var a = 0; a <= max; a=a+10) {
        for (var b = 0; b <= max; b = b+10) {
            for (var c = 0; c <= max; c=c+10) {
                ncs = "NCS S " + num2(a) + num2(b) + "-Y" + num2(c) + "R" // ja
                if (ncs) {
                    rgb = ncsToRgbHash(ncs)
                    ncsMap.set(ncs, rgb)
                }
                ncs = "NCS S " + num2(a) + num2(b) + "-R" + num2(c) + "B" // ja
                if (ncs) {
                    rgb = ncsToRgbHash(ncs)
                    ncsMap.set(ncs, rgb)
                }
                ncs = "NCS S " + num2(a) + num2(b) + "-B" + num2(c) + "G" // ja
                if (ncs) {
                    rgb = ncsToRgbHash(ncs)
                    ncsMap.set(ncs, rgb)
                }

                ncs = "NCS S " + num2(a) + num2(b) + "-G" + num2(c) + "Y" // ja
                if (ncs) {
                    rgb = ncsToRgbHash(ncs)
                    ncsMap.set(ncs, rgb)
                }
            }
        }
    }

    console.log("check ", ncsMap.size, " colors")
    console.log("ncsMap", ncsMap)


    //colorToFind = "#ffffff"
    //needle = "#fff095"
    //needle = "#36486b"

    //needle = "#b0a896" //needed
    //needle = "#7fa095" //needed

    foundMap = new Map()
    deltaMap = new Map()
    ncsMap.forEach((ncs, rgb) => {
        if (rgb == colorToFind) {
            console.log(ncs, "->", rgb)
            found.set(ncs, rgb)
        }

        if (ncsToRgb(ncs) != NaN) {
            delta = colorDelta(rgb, colorToFind)
            deltaMap.set(delta, rgb)
        }

    });

    console.log("delta:", deltaMap.size)
    console.log("found:", foundMap.size)
    foundList = ""
    nearbyList = ""

    if (foundMap.size >= 1)
        foundMap.forEach((rgb, ncs) => {
            foundList += "\n" + ncs + " -> " + rgb + " <br/>"
        });
    console.log(foundMap)


    deltaMap = new Map([...deltaMap.entries()].sort())
    console.log(deltaMap)

    i = 25;
    deltaMap.forEach((delta, ncs) => {
        if (i > 0) {
            i--

            nearbyList += + ncs + "  -  " + delta + "  -  " + 00 + "<br/> \n"
        }
    })

    return { foundList, nearbyList }
}


function rgbTripel(colorHash) {
    // if (isColorHash(colorHash)) {
    //    console.error("not a Hash -> " + colorHash)
    //    return NaN
    // }
    r = hexToDecimal(colorHash.slice(1, 3))
    g = hexToDecimal(colorHash.slice(3, 5))
    b = hexToDecimal(colorHash.slice(5, 7))


    //return ("r: " + r + " g: " + g + " b: " + b)
    return {
        r: r,
        g: g,
        b: b
    };
}

function qA(x, y) {
    bstnd = (x - y)
    return bstnd * bstnd;
}

function isColorHash(myVar) {


    if (!isNaN(myVar))
        return false
    if (!(typeof myVar === 'string' || myVar instanceof String))
        return false
    if (!myVar.slice(0, 1) == "#")
        return false;

    return true

}

function colorDelta(x, y) {
    if (!isColorHash(x))
        return Infinity;
    if (!isColorHash(y))
        return Infinity;
    if (x === y)
        return 0;
    x = rgbTripel(x)
    y = rgbTripel(y)
    qa = qA(x.r, y.r) + qA(x.g, y.g) + qA(x.b, y.b)
    qa = Math.sqrt(qa)
    return  qa
}